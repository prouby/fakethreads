#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>

#define MAX_FT_NB 100
#define INIT_DFL_DELAY -1

#define SAVE_ENV()                                                      \
  do                                                                    \
    {                                                                   \
      set_start_safe_zone ();                                           \
      if (sigsetjmp(*(get_current_env()), 1) == 0)                      \
        {                                                               \
          ft[cur_ft_id].jmp = 0;                                        \
        }                                                               \
      else                                                              \
        {                                                               \
          ft[cur_ft_id].jmp += 1;                                       \
          if (ft[cur_ft_id].jmp > 1)                                    \
            fprintf (stderr,                                            \
                     "[WARN] [%d] More than one reset at point"         \
                     " %s: %s (%d)\n",                                  \
                     cur_ft_id, __FILE__, __func__, __LINE__);          \
        }                                                               \
      set_end_safe_zone ();                                             \
    }                                                                   \
  while(0);

typedef void (*fthread_fun_t) ();

struct fthread
{
  int id;                       /* Id of threads */
  int runned;                   /* Running status */
  sigjmp_buf env;               /* Last save point */
  fthread_fun_t fun;            /* Function pointer*/
  void * data;                  /* Function data */
  int jmp;
};
typedef struct fthread fthread_t;

int cur_ft_id;
int nb_ft = 0;
fthread_t ft[MAX_FT_NB];
sigjmp_buf init_env, run;
sigset_t mask;

int sig_delay = 5000;

int
init_fakethreads (int delay)
{
  sigaddset (&mask, SIGALRM);

  if (delay > 0)
    sig_delay = delay;

  return 0;
}

sigjmp_buf *
get_current_env ()
{
  return (sigjmp_buf *)&ft[cur_ft_id].env;
}

void *
get_ft_data ()
{
  return (void *)ft[cur_ft_id].data;
}

int
add_thread (fthread_fun_t fun, void * data)
{
  if (fun == NULL)
    return -1;

  if (nb_ft >= MAX_FT_NB)
    return -1;

  fthread_t * cur = &ft[nb_ft];

  cur->id = nb_ft;
  cur->runned = 0;
  cur->fun = fun;
  cur->data = data;

  nb_ft++;

  return cur->id;
}

int
rm_thread (int id)
{
  fthread_t * cur = &ft[id];
  cur->id = -1;

  return 0;
}

void
init_handler (int sig)
{
  siglongjmp (init_env, 1);
}

void
alarm_handler (int sig)
{
  int x;
  fthread_t * cur;

  cur = &ft[++cur_ft_id];
  x = 0;
  while (cur->fun == NULL || cur->id < 0)
    {
      if (x > 3*nb_ft)
        {
          cur = NULL;
          break;
        }

      if (cur_ft_id >= nb_ft)
        cur_ft_id = -1;

      cur = &ft[++cur_ft_id];
      x += 1;
    }

  if (cur == NULL)
    {
      /* printf ("Add thread ended\n"); */
      siglongjmp(run, 1);
    }

  /* printf ("Switch to fake thread %d\n", cur_ft_id); */

  /* If cur as always jmp 2 time or more on same save spot, give more
     time. */
  if (cur->jmp > 1)
    ualarm (sig_delay*cur->jmp, 0);
  else
    ualarm (sig_delay, 0);

  siglongjmp(cur->env, 1);
}

void
set_start_safe_zone ()
{
  sigprocmask (SIG_BLOCK, &mask, NULL);
}

void
set_end_safe_zone ()
{
  sigprocmask (SIG_UNBLOCK, &mask, NULL);
}

int
run_threads ()
{
  sigprocmask (SIG_BLOCK, &mask, NULL);

  struct sigaction sa, old;
  sa.sa_handler = init_handler;
  sa.sa_flags = 0;

  sigaction (SIGALRM, &sa, &old);

  cur_ft_id = 0;

  volatile int x = 0;
  fthread_t * cur;

  sigprocmask (SIG_UNBLOCK, &mask, NULL);

  /* This is signal loop ... */
  sigsetjmp (init_env, 1);
  cur = &ft[x++];
  if (cur != NULL && cur->id >= 0 && cur->fun != NULL)
    {
      ualarm (sig_delay, 0);
      /* printf ("Run fake thread %d\n", cur->id); */
      cur->runned = 1;
      cur_ft_id = x-1;
      cur->fun ();
    }

  sigprocmask (SIG_BLOCK, &mask, NULL);

  sa.sa_handler = alarm_handler;
  sigaction (SIGALRM, &sa, NULL);
  cur = &ft[0];

  sigprocmask (SIG_UNBLOCK, &mask, NULL);

  /* printf ("Jmp to thread 0\n"); */

  if (sigsetjmp (run, 1) == 0)
    {
      cur_ft_id = 0;
      ualarm (sig_delay, 0);
      siglongjmp (cur->env, 1);
    }
  return 0;
}

void
t_fun_1 ()
{
  register int x;
  SAVE_ENV();
  printf ("\t[%d] Function 1 running...\n", cur_ft_id);

  SAVE_ENV();
  for (x = 0; x < 1000000; x++);

  printf ("\t[%d] 1\n", cur_ft_id);

  SAVE_ENV();
  for (x = 0; x < 1000000; x++);

  printf ("\t[%d] 2\n", cur_ft_id);

  SAVE_ENV();
  for (x = 0; x < 1000000; x++);

  printf ("\t[%d] 3\n", cur_ft_id);

  SAVE_ENV();
  for (x = 0; x < 1000000; x++);

  printf ("\t[%d] 4\n", cur_ft_id);

  SAVE_ENV();
  for (x = 0; x < 1000000; x++);

  printf ("\t[%d] 5\n", cur_ft_id);

  SAVE_ENV();
  for (x = 0; x < 1000000; x++);


  SAVE_ENV();
  printf ("\t[%d] Function 1 end\n", cur_ft_id);

  ft[cur_ft_id].id = -1;
  ualarm (1, 0);
  pause();
}

void
t_fun_2 ()
{
  register int x, y = 0;
  SAVE_ENV();
  printf ("\t[%d] Function 2 running...\n", cur_ft_id);

  set_start_safe_zone ();
  while (y < 16)
    {
      for (x = 0; x < 1000000; x++);

      y += 1;

      printf ("\t[%d] +++ %s %d\n", cur_ft_id, (char *)get_ft_data(), y);
    }

  SAVE_ENV();
  set_end_safe_zone ();

  /* Not safe env y is not safe */
  y = 0;
  while (y < 16)
    {
      SAVE_ENV();
      for (x = 0; x < 1000000; x++);

      y += 1;

      printf ("\t[%d] --- %s %d\n", cur_ft_id, (char *)get_ft_data(), y);
    }

  /* SAVE_ENV(); */
  printf ("\t[%d] Function 2 end\n", cur_ft_id);

  ft[cur_ft_id].id = -1;
  ualarm (1, 0);
  pause();
}


int
main (int argc, char **argv)
{
  int i, n;
  if (argc < 2)
    n = 5;
  else
    n = atoi(argv[1]);

  srand(time(NULL));
  int r = (rand()%800) - 400;   /* Randomize thread time */

  printf ("Set delay to %d\n", 6000 + r);
  init_fakethreads (6000 + r);

  for (i = 0; i < n; i++)
    printf ("Install thread %d\n", add_thread (t_fun_1, NULL));

  char * str = "xxx";
  printf ("Install thread %d\n", add_thread (t_fun_2, (void *)str));

  char * str2 = "yyy";
  printf ("Install thread %d\n", add_thread (t_fun_2, (void *)str2));

  run_threads();

  printf ("End of main\n");

  return 0;
}
